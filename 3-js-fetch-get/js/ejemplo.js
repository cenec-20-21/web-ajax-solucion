(() => {

    var btnGet = document.getElementById("btnGet");

    // Opción A: 
    btnGet.onclick = () => {
        var url = document.getElementById("url").value;
        fetch(url)
            .then(response => response.text())
            .then(text => {
                let resultadoSection = document.getElementById("resultado");
                resultadoSection.innerHTML = text;
            });
        
    };

    // Opción B: Con async await
    // btnGet.onclick = async () => {
    //     var url = document.getElementById("url").value;
    //     const response = await fetch(url);
    //     let text = await response.text();
    //     let resultadoSection = document.getElementById("resultado");
    //     resultadoSection.innerHTML = text;
    // };
    
})();