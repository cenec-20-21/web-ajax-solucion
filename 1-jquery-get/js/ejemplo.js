$(document).ready(()=>{

    let btnGet = $("button");
    btnGet.click(()=> {
        let urlValue = $("input").val();
        if(!urlValue || 0 === urlValue.length){
            alert("Por favor introduce una url válida");
            return;
        }
        
        // Opción A: El método "fluent". Ver https://api.jquery.com/jquery.get/
        $.get(urlValue, (data, status, xhr) => {
            let statusCode = xhr.status;
            let statusText = xhr.statusText;
            $("#resultado").html(statusCode + " " + statusText + ": " + data);
        });

        // Opción B: El método "clásico"
        // let request = {
        //     url: urlValue,
        //     dataType: 'json',
        //     success: data => {
        //         $("#resultado").html(data);
        //     }
        // };
        // $.ajax(request);
    });

});
